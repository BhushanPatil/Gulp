'use strict';

var gulp = 			require('gulp'),
	sass = 			require('gulp-sass'),
	postcss = 		require('gulp-postcss'),
	autoprefixer = 	require('autoprefixer'),
	cssnano = 		require('cssnano'),
	mqpacker = 		require('css-mqpacker'),
	browserSync = 	require('browser-sync').create();

gulp.task('css', function(){
	var sassConfig = {
		outputStyle: 'expanded',
		precision: 10
	};
	var postcssPlugins = [
		autoprefixer({browsers: ['last 2 version']}),
		mqpacker(),
		// cssnano()
    ];
	return gulp.src('./*.scss')
		.pipe(sass(sassConfig).on('error', sass.logError))
		.pipe(postcss(postcssPlugins))
		.pipe(gulp.dest('./'));
});

gulp.task('serve', function() {
	browserSync.init({
		server: './',
		port: 80,
		browser: 'chrome',
		open: false
	});
	gulp.watch('./*.scss', ['css']);
	gulp.watch('./*.css', function(){ gulp.src('./*.css').pipe(browserSync.stream()); });
	gulp.watch(['./*.html','./*.js']).on('change', browserSync.reload);
});

// Default gulp task
gulp.task('default', ['serve']);